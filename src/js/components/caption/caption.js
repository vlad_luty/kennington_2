document.addEventListener('scroll', (evnt) => {
    document.querySelectorAll('.section').forEach((section) => {
        let scroll =  (window.pageYOffset || document.scrollTop)  - (document.clientTop || 0);


        if (isNaN(scroll) === false) {
            if ((scroll + window.innerHeight / 2) > section.offsetTop) {
                section.querySelector('.section__caption').classList.add('caption--underlined');
            }
        }


    })
});