$('.cloud__link').click(function() {
    let sectionTo = $(this).attr('href');

    let body = $(".page, .page__body");
    body.stop().animate({scrollTop:$(sectionTo).offset().top}, 1500, 'swing');
});