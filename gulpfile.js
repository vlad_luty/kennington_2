'use strict';

const gulp         = require('gulp'),                           // подключаем Gulp
    webserver      = require('browser-sync'),                   // сервер для работы и автоматического обновления страниц
    plumber        = require('gulp-plumber'),                   // модуль для отслеживания ошибок Gulp
    rigger         = require('gulp-rigger'),                    // модуль для импорта содержимого одного файла в другой
    sourcemaps     = require('gulp-sourcemaps'),                // модуль для генерации карты исходных файлов
    sass           = require('gulp-sass'),                      // модуль для компиляции SASS (SCSS) в CSS
    postcss        = require('gulp-postcss'),                   // postCss
    autoprefixer   = require('autoprefixer'),                   // модуль для автоматической установки автопрефиксов
    cleanCSS       = require('gulp-clean-css'),                 // плагин для минимизации CSS
    uglify         = require('gulp-uglify-es').default,         // модуль для минимизации JavaScript
    cache          = require('gulp-cache'),                     // модуль для кэширования
    imagemin       = require('gulp-imagemin'),                  // плагин для сжатия PNG, JPEG, GIF и SVG изображений
    webp           = require('imagemin-webp'),
    jpegrecompress = require('imagemin-jpeg-recompress'),       // плагин для сжатия jpeg
    pngquant       = require('imagemin-pngquant'),              // плагин для сжатия png
    rename         = require('gulp-rename'),
    htmlmin        = require('gulp-htmlmin'),
    htmlhint       = require("gulp-htmlhint"),
    del            = require('del'),                            // плагин для удаления файлов и каталогов
    pug            = require('gulp-pug'),
    stylus         = require('gulp-stylus'),
    stylelint      = require('gulp-stylelint'),
    cssnano        = require('cssnano'),
    postfocus      = require('postcss-focus'),
    postshort      = require('postcss-short'),
    lost           = require('lost'),
    mqpacker       = require("css-mqpacker"),
    cssnext        = require('postcss-cssnext'),
    concat         = require('gulp-concat'),
    ttf2woff       = require('gulp-ttf2woff'),
    ttf2woff2      = require('gulp-ttf2woff2'),
    ttf2svg        = require('gulp-ttf-svg'),
    ttf2eot        = require('gulp-ttf2eot'),
    consolidate    = require('gulp-consolidate'),
    iconfont       = require('gulp-iconfont'),
    gulpif         = require('gulp-if');

var argv = require('yargs').argv;


var autoprefixerList = [
    'Chrome >= 45',
    'Firefox ESR',
    'Edge >= 12',
    'Explorer >= 10',
    'iOS >= 9',
    'Safari >= 9',
    'Android >= 4.4',
    'Opera >= 30'
];

var isProduction = (argv.production !== undefined);

gulp.task('fonts:icons', function () {
    return gulp.src('src/fonts/icons/**/*.svg')
        .pipe(plumber())
        .pipe(iconfont({
            fontName: 'iconfont',
            formats: ['woff', 'woff2'],
            appendCodepoints: true,
            appendUnicode: false,
            normalize: true,
            fontHeight: 1000,
            centerHorizontally: true
        }))
        .on('glyphs', function (glyphs, options) {
            gulpif(
                !isProduction,
                gulp.src('src/sass/components/icon/_icon-template.scss')
                    .pipe(consolidate('underscore', {
                        glyphs: glyphs,
                        fontName: options.fontName,
                        fontDate: new Date().getTime()
                    }))
                    .pipe(rename('_icon.scss'))
                    .pipe(gulp.dest('src/sass/components/icon'))
                    .pipe(gulp.src('src/sass/components/icon/_icon-template.html'))
                    .pipe(consolidate('underscore', {
                        glyphs: glyphs,
                        fontName: options.fontName
                    }))
                    .pipe(rename('_icon.html'))
                    .pipe(gulp.dest('src/sass/components/icon'))
            );
        })
        .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('fonts:ttf2woff', function(){
    return gulp.src(['src/fonts/**/*.ttf'])
        .pipe(ttf2woff())
        .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('fonts:ttf2woff2', function(){
    return gulp.src(['src/fonts/**/*.ttf'])
        .pipe(ttf2woff2())
        .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('fonts:ttf2svg', function(){
    return gulp.src(['src/fonts/**/*.ttf'])
        .pipe(ttf2svg())
        .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('fonts:ttf2eot', function(){
    return gulp.src(['src/fonts/**/*.ttf'])
        .pipe(ttf2eot())
        .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('fonts:convert', gulp.parallel('fonts:ttf2woff', 'fonts:ttf2woff2', 'fonts:ttf2svg', 'fonts:ttf2eot', () => {
    return gulp.src(['src/fonts/**/*.ttf'])
        .pipe(gulp.dest('dist/assets/fonts'));
}));

gulp.task('img:webp', () => {
    return gulp.src('src/img/**/*.{png,jpg,jpeg}')
        .pipe(imagemin({
            verbose: true,
            plugins: webp({
                quality: 75,
                // lossless: true
            })
        }))
        .pipe(rename((path) => { path.extname = ".webp" }))
        .pipe(gulp.dest('dist/assets/images'));
});

gulp.task('img:compress', gulp.series('img:webp', () => {
    return gulp.src('src/img/**/*.{svg,gif,png,jpg,jpeg}')
        .pipe(plumber())
        .pipe(imagemin([
            jpegrecompress({
                progressive: true,
                max: 80,
                min: 70
            }),
            imagemin.gifsicle({interlaced: true}),
            imagemin.optipng(),
            pngquant({quality: [0.7, 0.8]}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest('dist/assets/images'))
}));

gulp.task('js', () => {
    return gulp.src('src/js/main.js')
        .pipe(plumber())
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist/assets/js'));
});

gulp.task('css:sass', () => {
    return gulp.src('src/sass/!(_)*.{scss,sass}')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'uncompressed'
        }))
        .pipe(postcss([
            lost(),
            mqpacker({sort : true}),
            postfocus(),
            postshort(),
            autoprefixer({
                overrideBrowserslist: autoprefixerList
            }),
            cssnano({
                minifyFontWeight: false,
                calc: {precision: 4}
            })
        ]))
        .pipe(sourcemaps.write('.'))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist/assets/css'));
});

gulp.task('html:pug', () => {
    return gulp.src('src/pug/**/!(_)*.pug')
        .pipe(plumber())
        .pipe(pug({pretty : true}))
        .pipe(gulp.dest('./'));
});

gulp.task('browser-sync', () => {
    webserver({
        injectChanges: true,
        server: {
            baseDir : './'
        },
        port : 3031
    });
});

gulp.task('watch:css', () => {
    gulp.watch('src/**/*.scss', {delay: 100}, gulp.series('css:sass', (done) => {
        webserver.reload();
        done();
    }));
});

gulp.task('watch:js', () => {
    gulp.watch('src/**/*.js', gulp.series('js', (done) => {
        webserver.reload();
        done();
    }));
});

gulp.task('watch:html', () => {
    gulp.watch('index.html', (done) => {
        webserver.reload();
        done();
    });
});

gulp.task('watch', gulp.parallel('browser-sync', 'watch:css', 'watch:js', 'watch:html'));

gulp.task('build', gulp.parallel('fonts:icons', 'fonts:convert', 'img:compress', 'img:webp','css:sass', 'js'));

gulp.task('default', gulp.parallel('build', 'watch'));